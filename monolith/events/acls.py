from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    city_name = city.split(" ")
    if len(city_name) > 1:
        city1 = city_name[0]
        city2 = city_name[1]
        url = (
            f"https://api.pexels.com/v1/search?query={city1}%20{city2}&{state}"
        )
    else:
        url = f"https://api.pexels.com/v1/search?query={city}&{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    # Use requests to get a fun fact
    response = json.loads(r.content)
    photos = response.get("photos")
    photo = photos[0].get("src").get("original")
    # picture_url = {"picture_url": photo}
    try:
        return {"picture_url": photo}
    except:
        return {"picture_url": None}
    # Create a dictionary of data to use containing

    # Return the dictionary


def get_weather(city, state):
    # params = {
    #     "q": city +","+ state+",USA"
    #     "appid": OPEN_WEATHER_API_KEY
    #     "limit": 1
    # }
    latlon = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(latlon)
    location = json.loads(response.content)
    lat = location[0]["lat"]
    lon = location[0]["lon"]

    weatherurl = f"https://api.openweathermap.org/data/2.5/weather?units=imperial&lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(weatherurl)
    weather = json.loads(response.content)

    try:
        return {
            "temperature": weather["main"]["temp"],
            "description": weather["weather"][0]["description"],
        }
    except:
        return None
